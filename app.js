const express = require("express");
const helmet = require("helmet");
const cors = require("cors");
const morgan = require("morgan");
const rateLimit = require("express-rate-limit");

const app = express();

app.use(helmet());

//cors
const corsOptions = {
  origin: "https://allowed-origin.com",
  methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
  optionsSuccessStatus: 204,
};
app.use(cors(corsOptions));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(morgan("dev"));

//rate limiter
const limiter = rateLimit({
  windowMs: 15 * 60 * 1000,
  max: 100,
});
app.use(limiter);

//helmet
app.use(
  helmet.contentSecurityPolicy({
    directives: {
      defaultSrc: ["'self'"],
      scriptSrc: ["'self'", "trusted-scripts.com"],
    },
  })
);

//api get method
app.get("/api/data", (req, res) => {
  res.json({
    message: "This is a secure GET method with additional security features.",
  });
});

// Rest api methods
app.get("/api/test", (req, res) => {
  res.json({
    message: "This is sample test message created by Amila dissanayake.",
  });
});

//http headers
const PORT = process.env.PORT || 3000;

if (process.env.NODE_ENV === "production") {
  app.use((req, res, next) => {
    if (
      req.headers["x-forwarded-proto"] &&
      req.headers["x-forwarded-proto"] !== "https"
    ) {
      return res.redirect("https://" + req.headers.host + req.url);
    }
    return next();
  });
}

// powerup server
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
